import apache_beam as beam
import numpy as np
import sys

def count_divisors(x):
   if x <= 1:
      return 1
   count = 2
   for i in range(2, x):
      if x % i == 0:
         count += 1
   return (x, count)

def main():
    p = beam.Pipeline(argv=sys.argv)
    output_prefix = '/tmp/beam/output'
    data = range(100000, 101000)

    (p
       | 'Create range' >> beam.Create(data)
       | 'Get divisors' >> beam.Map(lambda x: count_divisors(x))
       | 'Write' >> beam.io.WriteToText(output_prefix)
    )

    p.run()

if __name__ == '__main__':
    main()
