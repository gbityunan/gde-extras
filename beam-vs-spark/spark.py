import numpy as np

def count_divisors(x):
   if x <= 1:
      return 1
   count = 2
   for i in range(2, x):
      if x % i == 0:
         count += 1
   return (x, count)

data = range(100000, 105000)
dist_data = sc.parallelize(data)
div_counts = dist_data.map(count_divisors)
print div_counts.collect()
